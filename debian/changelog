pyfr (1.15.0-1) UNRELEASED; urgency=medium

  * Team upload

  [ Edward Betts ]
  * Upstream GitHub repository has moved, update URLs

  [ Andreas Tille ]
  * New upstream version
    Closes: #997427
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * Use secure URI in Homepage field.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.
  * watch file standard 4 (routine-update)
  * Build-Depends: python3-sphinx-rtd-theme
  * sphinxcontrib.contentui is not packaged
  * Upstream does not provide examples any more
  * Fix doc-base
  * Drop non-existing file from d/copyright
  * Add examples from old version to debian/ dir
  * Fix autopkgtest scripts
  * Test-Depends: @builddeps@
  * Depends: python3-h5py

 -- Andreas Tille <tille@debian.org>  Mon, 16 Jan 2023 21:10:55 +0100

pyfr (1.5.0-3) unstable; urgency=medium

  * Team upload.
  * Fix installation of manpage
    Closes: #922257

 -- Andreas Tille <tille@debian.org>  Tue, 05 Mar 2019 20:34:01 +0100

pyfr (1.5.0-2) unstable; urgency=medium

  * Team upload.
  * Point Vcs-URLs to Salsa
  * Standards-Version: 4.1.4
  * Fix issue with recent sphinx versions
    Closes: #896485
  * Drop ancient python version field X-Python3-Version: >= 3.3

 -- Andreas Tille <tille@debian.org>  Sun, 20 May 2018 06:47:05 +0200

pyfr (1.5.0-1) unstable; urgency=medium

  * Track GitHub instead of PyPI releases.
  * Refresh patch queue.
  * Add missing build dependencies on h5py, mako, numpy and pytools.
  * Add new build dependency on gimmik.
  * Upgrade packaging to debhelper 10.
  * Drop superfluous Testsuite field.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 15 Nov 2016 19:17:58 +0000

pyfr (1.4.0-3) unstable; urgency=medium

  * Build and distribute the manpages.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 28 May 2016 15:43:40 +0100

pyfr (1.4.0-2) unstable; urgency=medium

  * Install pyfr module to private location.
    Thanks to Piotr Ozarowski
  * Drop unnecessary build dependencies.
  * Drop install dependency on mpi-default-bin.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 12 May 2016 20:28:24 +0100

pyfr (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control: replace install dependency on gcc by build-essential.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 21 Apr 2016 08:13:51 +0100

pyfr (1.3.0-3) unstable; urgency=medium

  * Add missing install dependency on gcc.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 20 Apr 2016 16:21:13 +0100

pyfr (1.3.0-2) unstable; urgency=medium

  * Add missing copyright information.
    Thanks to Chris Lamb
  * Work around Debian bug #821466:
    - d/control: pyfr now explicitly depends on mpi-default-bin instead of
      expecting it to be pulled transitively via mpi4py.
    - d/tests/control: simplified and refactored.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 18 Apr 2016 21:22:20 +0100

pyfr (1.3.0-1) unstable; urgency=low

  * Initial release. (Closes: #821008)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 16 Apr 2016 20:47:39 +0100
